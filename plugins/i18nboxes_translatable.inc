<?php

/**
 * Translatable simple custom text box.
 */
class i18nboxes_translatable extends boxes_simple {


  /**
   * Implementation of boxes_content::options_form().
   */
  public function options_form() {
    $form = parent::options_form();

    $form['translation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Translation'),
      'message' => array(
        '#prefix' => '<div class="content">',
        '#suffix' => '</div>',
      ),
    );

    if (!empty($this->delta)) {
      $source_title = i18nstrings_get_source("blocks:boxes:{$this->delta}:title");
      $source_body = i18nstrings_get_source("blocks:boxes:{$this->delta}:body");

      // Build the translation links.
      $t_args = array(
        '%translate_interface' => t('Translate interface'),
        '!links' => theme('item_list', array(
          (!empty($source_title->lid)) ? l('Go to title translation', 'admin/build/translate/edit/'. $source_title->lid) : t('Title translation not yet available. Therefore you have to enter a title and save this box.'),
          (!empty($source_body->lid)) ? l('Go to body translation', 'admin/build/translate/edit/'. $source_body->lid) : t('Body translation not yet available. Therefore you have to enter a body text and save this box.'),
        )),
      );
      $form['translation']['message']['#value'] = t('This box can be translated in other languages in the %translate_interface section: !links', $t_args);
    }
    else {
      $form['translation']['message']['#value'] = t('After saving the box, you can translate it to other languages in the !translate_interface section.', array('!translate_interface' => l(t('Translate interface'), 'admin/build/translate')));
    }
    return $form;
  }


  /**
   * Override the save function to udpate i18nstrings
   */
  public function save() {
    parent::save();
    // Only process if box has been saved.
    if ($this->new === FALSE && isset($this->options['body'], $this->options['format'])) {
      i18nstrings_update("blocks:boxes:{$this->delta}:title", isset($this->title) ? $this->title : '');
      i18nstrings_update("blocks:boxes:{$this->delta}:body", $this->options['body'],  $this->options['format']);
    }
  }

  /**
   * Implementation of boxes_content::options_form().
   */
  public function render() {
    $content = '';
    if (isset($this->delta)) {
      $content = i18nboxesstrings_text("blocks:boxes:{$this->delta}:body", $this->options['body'], $this->options['format']);
    }
    $title = i18nstrings("blocks:boxes:{$this->delta}:title", $this->title);
    return array(
      'delta' => $this->delta, // Crucial.
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}